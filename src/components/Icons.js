import React, { Component } from 'react';

export class CallRounded extends Component {
  render() {
    let fs = "",fc="MuiSvgIcon-root";
    if(this.props.fontSize){
        fs = this.props.fontSize;
        if(fs ==="large"){
            fc += " MuiSvgIcon-fontSizeLarge"
        }else if(fs == "small"){
            fc += " MuiSvgIcon-fontSizeSmall"
        }
    }
    return(
        <svg className={fc} focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
            <path d="M19.23 15.26l-2.54-.29c-.61-.07-1.21.14-1.64.57l-1.84 1.84c-2.83-1.44-5.15-3.75-6.59-6.59l1.85-1.85c.43-.43.64-1.03.57-1.64l-.29-2.52c-.12-1.01-.97-1.77-1.99-1.77H5.03c-1.13 0-2.07.94-2 2.07.53 8.54 7.36 15.36 15.89 15.89 1.13.07 2.07-.87 2.07-2v-1.73c.01-1.01-.75-1.86-1.76-1.98z"></path>
        </svg>
    )
  }
}


export class MicRounded extends Component {
    render() {
      let fs = "",fc="MuiSvgIcon-root";
      if(this.props.fontSize){
          fs = this.props.fontSize;
          if(fs ==="large"){
              fc += " MuiSvgIcon-fontSizeLarge"
          }else if(fs == "small"){
              fc += " MuiSvgIcon-fontSizeSmall"
          }
      }
      return(
        <svg className={fc} focusable="false" viewBox="0 0 24 24" aria-hidden="true">
            <path d="M12 14c1.66 0 3-1.34 3-3V5c0-1.66-1.34-3-3-3S9 3.34 9 5v6c0 1.66 1.34 3 3 3zm-1-9c0-.55.45-1 1-1s1 .45 1 1v6c0 .55-.45 1-1 1s-1-.45-1-1V5zm6.91 6c-.49 0-.9.36-.98.85C16.52 14.2 14.47 16 12 16s-4.52-1.8-4.93-4.15c-.08-.49-.49-.85-.98-.85-.61 0-1.09.54-1 1.14.49 3 2.89 5.35 5.91 5.78V20c0 .55.45 1 1 1s1-.45 1-1v-2.08c3.02-.43 5.42-2.78 5.91-5.78.1-.6-.39-1.14-1-1.14z"></path>
        </svg>
      )
    }
}

export class CallEndRounded extends Component {
    render() {
      let fs = "",fc="MuiSvgIcon-root";
      if(this.props.fontSize){
          fs = this.props.fontSize;
          if(fs ==="large"){
              fc += " MuiSvgIcon-fontSizeLarge"
          }else if(fs == "small"){
              fc += " MuiSvgIcon-fontSizeSmall"
          }
      }
      return(
        <svg className={fc} focusable="false" viewBox="0 0 24 24" aria-hidden="true">
            <path d="M4.51 15.48l2-1.59c.48-.38.76-.96.76-1.57v-2.6c3.02-.98 6.29-.99 9.32 0v2.61c0 .61.28 1.19.76 1.57l1.99 1.58c.8.63 1.94.57 2.66-.15l1.22-1.22c.8-.8.8-2.13-.05-2.88-6.41-5.66-16.07-5.66-22.48 0-.85.75-.85 2.08-.05 2.88l1.22 1.22c.71.72 1.85.78 2.65.15z"></path>
        </svg>
      )
    }
}

export class VideocamRounded extends Component {
    render() {
      let fs = "",fc="MuiSvgIcon-root";
      if(this.props.fontSize){
          fs = this.props.fontSize;
          if(fs ==="large"){
              fc += " MuiSvgIcon-fontSizeLarge"
          }else if(fs == "small"){
              fc += " MuiSvgIcon-fontSizeSmall"
          }
      }
      return(
        <svg className={fc} focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M17 10.5V7c0-.55-.45-1-1-1H4c-.55 0-1 .45-1 1v10c0 .55.45 1 1 1h12c.55 0 1-.45 1-1v-3.5l2.29 2.29c.63.63 1.71.18 1.71-.71V8.91c0-.89-1.08-1.34-1.71-.71L17 10.5z"></path></svg>
      )
    }
}

export class Comment extends Component {
    render() {
      let fs = "",fc="MuiSvgIcon-root";
      if(this.props.fontSize){
          fs = this.props.fontSize;
          if(fs ==="large"){
              fc += " MuiSvgIcon-fontSizeLarge"
          }else if(fs == "small"){
              fc += " MuiSvgIcon-fontSizeSmall"
          }
      }
      return(
        <svg className={fc} focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18zM17 14H7c-.55 0-1-.45-1-1s.45-1 1-1h10c.55 0 1 .45 1 1s-.45 1-1 1zm0-3H7c-.55 0-1-.45-1-1s.45-1 1-1h10c.55 0 1 .45 1 1s-.45 1-1 1zm0-3H7c-.55 0-1-.45-1-1s.45-1 1-1h10c.55 0 1 .45 1 1s-.45 1-1 1z"></path></svg>
      )
    }
}

export class VideocamOffRounded extends Component {
    render() {
      let fs = "",fc="MuiSvgIcon-root";
      if(this.props.fontSize){
          fs = this.props.fontSize;
          if(fs ==="large"){
              fc += " MuiSvgIcon-fontSizeLarge"
          }else if(fs == "small"){
              fc += " MuiSvgIcon-fontSizeSmall"
          }
      }
      return(
        <svg className={fc} focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M21 14.2V8.91c0-.89-1.08-1.34-1.71-.71L17 10.5V7c0-.55-.45-1-1-1h-5.61l8.91 8.91c.62.63 1.7.18 1.7-.71zM2.71 2.56c-.39.39-.39 1.02 0 1.41L4.73 6H4c-.55 0-1 .45-1 1v10c0 .55.45 1 1 1h12c.21 0 .39-.08.55-.18l2.48 2.48c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L4.12 2.56a.9959.9959 0 00-1.41 0z"></path></svg>
      )
    }
}

export class MicOffRounded extends Component {
    render() {
      let fs = "",fc="MuiSvgIcon-root";
      if(this.props.fontSize){
          fs = this.props.fontSize;
          if(fs ==="large"){
              fc += " MuiSvgIcon-fontSizeLarge"
          }else if(fs == "small"){
              fc += " MuiSvgIcon-fontSizeSmall"
          }
      }
      return(
        <svg className={fc} focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M15 10.6V5c0-1.66-1.34-3-3-3-1.54 0-2.79 1.16-2.96 2.65L15 10.6zm3.08.4c-.41 0-.77.3-.83.71-.05.32-.12.64-.22.93l1.27 1.27c.3-.6.52-1.25.63-1.94.07-.51-.33-.97-.85-.97zM3.71 3.56c-.39.39-.39 1.02 0 1.41L9 10.27v.43c0 1.19.6 2.32 1.63 2.91.75.43 1.41.44 2.02.31l1.66 1.66c-.71.33-1.5.52-2.31.52-2.54 0-4.88-1.77-5.25-4.39-.06-.41-.42-.71-.83-.71-.52 0-.92.46-.85.97.46 2.96 2.96 5.3 5.93 5.75V20c0 .55.45 1 1 1s1-.45 1-1v-2.28c.91-.13 1.77-.45 2.55-.9l3.49 3.49c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L5.12 3.56a.9959.9959 0 00-1.41 0z"></path></svg>
      )
    }
}

export class ViewColumnRounded extends Component {
    render() {
      let fs = "",fc="MuiSvgIcon-root";
      if(this.props.fontSize){
          fs = this.props.fontSize;
          if(fs ==="large"){
              fc += " MuiSvgIcon-fontSizeLarge"
          }else if(fs == "small"){
              fc += " MuiSvgIcon-fontSizeSmall"
          }
      }
      return(
        <svg className={fc} focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M11 18h3c.55 0 1-.45 1-1V6c0-.55-.45-1-1-1h-3c-.55 0-1 .45-1 1v11c0 .55.45 1 1 1zm-6 0h3c.55 0 1-.45 1-1V6c0-.55-.45-1-1-1H5c-.55 0-1 .45-1 1v11c0 .55.45 1 1 1zM16 6v11c0 .55.45 1 1 1h3c.55 0 1-.45 1-1V6c0-.55-.45-1-1-1h-3c-.55 0-1 .45-1 1z"></path></svg>
      )
    }
}

export class PictureInPictureRounded extends Component {
    render() {
      let fs = "",fc="MuiSvgIcon-root";
      if(this.props.fontSize){
          fs = this.props.fontSize;
          if(fs ==="large"){
              fc += " MuiSvgIcon-fontSizeLarge"
          }else if(fs == "small"){
              fc += " MuiSvgIcon-fontSizeSmall"
          }
      }
      return(
        <svg className={fc} focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M18 7h-6c-.55 0-1 .45-1 1v4c0 .55.45 1 1 1h6c.55 0 1-.45 1-1V8c0-.55-.45-1-1-1zm3-4H3c-1.1 0-2 .9-2 2v14c0 1.1.9 1.98 2 1.98h18c1.1 0 2-.88 2-1.98V5c0-1.1-.9-2-2-2zm-1 16.01H4c-.55 0-1-.45-1-1V5.98c0-.55.45-1 1-1h16c.55 0 1 .45 1 1v12.03c0 .55-.45 1-1 1z"></path></svg>
      )
    }
}

export class CloseRounded extends Component {
    render() {
      let fs = "",fc="MuiSvgIcon-root";
      if(this.props.fontSize){
          fs = this.props.fontSize;
          if(fs ==="large"){
              fc += " MuiSvgIcon-fontSizeLarge"
          }else if(fs == "small"){
              fc += " MuiSvgIcon-fontSizeSmall"
          }
      }
      return(
        <svg className={fc} focusable="false" viewBox="0 0 24 24" aria-hidden="true"><path d="M18.3 5.71a.9959.9959 0 00-1.41 0L12 10.59 7.11 5.7a.9959.9959 0 00-1.41 0c-.39.39-.39 1.02 0 1.41L10.59 12 5.7 16.89c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0L12 13.41l4.89 4.89c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L13.41 12l4.89-4.89c.38-.38.38-1.02 0-1.4z"></path></svg>
      )
    }
}