import React, { Component, lazy,Suspense } from "react";
import Title from "antd/lib/typography/Title";
import { Switch, Route, Redirect } from "react-router-dom";
import { Loadings } from "../../../components/Loading";
import { Spin } from "antd";
const Show = lazy(()=> import('./Show'));
const DataInvoice = lazy(()=> import('./Data'));
const DataSender = lazy(()=> import('./Sender'));
class Invoice extends Component{
    
    render(){
        const { subPage } = this.props.match.params;
        return(
            <>
                
                <Suspense fallback={<div className="page-loading"><Spin size="large" /></div>}>
                    <Switch>
                        <Route path="/admin/invoice/sender" component={DataSender}  />
                        <Route path="/admin/invoice/show/:invoiceId?" component={Show}  />
                        <Route path="/admin/invoice/all" component={DataInvoice} />
                        <Redirect to="/admin/invoice/all" />
                    </Switch>
                </Suspense>
                
            </>
        )
    }
}

export default Invoice;