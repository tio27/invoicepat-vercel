import React, { Component, lazy, Suspense } from "react";
import Title from "antd/lib/typography/Title";
import { Switch, Route, Redirect, Link } from "react-router-dom";
import { myFirebase } from "../../../firebase/firebase";
import { Table, Menu, Breadcrumb } from "antd";
import $ from "jquery";
import moment from "moment";
    

class DataSender extends Component{
    state = {data:[]};
    column = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => a.name.localeCompare(b.name)
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            sorter: (a, b) => a.email.localeCompare(b.email)
        },
        {
            title: 'Country',
            dataIndex: 'country',
            key: 'country',
            sorter: (a, b) => moment(a.dateIssued,'MM/DD/YYYY').unix() - moment(b.dateIssued,'MM/DD/YYYY').unix(),
        },
        {
            title: 'Phone Number',
            dataIndex: 'phone',
            key: 'phone',
            sorter: (a, b) => a.phone.localeCompare(b.phone)
        },
        {
            title: 'Invoice Sent',
            dataIndex: 'sent',
            key: 'sent',
            sorter: (a, b) => a.sent - b.sent
        },
        {
            title: 'Total Invoice Value',
            dataIndex: 'total',
            key: 'total',
            align: 'right',
            sorter: (a, b) => a.total - b.total,
            render: (text,r,i) =>{
                return r.currencyCode+" "+this.currency(text)
            }
        },
        
    ]


    constructor(props){
        super(props);
        this.ref = myFirebase.firestore().collection('invoice').orderBy('created','desc');
        this.unsub = null;
    }

    dateFormat = (date, format = "MM/DD/YYYY") =>{
        return moment(date.seconds*1000).format(format)
    }

    hasValue = (array,val) => {
        console.log('check '+val+" = "+$.inArray(val,array ));
        if($.inArray(val,array) !== -1 ){
          return true;
        }else{
          return false;
        }
    }
    
    addValue = (array,email,total) =>{
        

        return array;
    }
    

    docUpdate = (querySnapshot) =>{
        console.log('docUpdate');
        const data = [],email=[];
        querySnapshot.forEach(doc => {
            const {created,emailFrom,billFrom,currencyCode,countryFrom,total,phoneFrom} = doc.data();
            if(!this.hasValue(email,emailFrom)){
                data.push({
                    key : doc.id,
                    name: billFrom,
                    email: emailFrom,
                    country: countryFrom,
                    created: this.dateFormat(created,"MM/DD/YYYY hh:mm:ss"),
                    phone: phoneFrom,
                    total: total,
                    currencyCode,
                    sent: 1
                })
                email.push(emailFrom)
            }else{
                Object.keys(data).map(function(i){
                    var d = data[i];
                    if(d.email === emailFrom){
                        d.sent = d.sent+1;
                        d.total=parseFloat(d.total)+parseFloat(total)
                    }
                })
            }
            
        });

        this.setState({data:data})
    }

    currency = (v=0,type="input") => {
        var b = v.toString().split('.'),
            val = b[0],
            c1 = b.length <= 1 ? ",00" : b[1]=="00"? ",00":","+b[1],
            min = v < 0 ? '-':'',
            n = parseInt(val.replace(/\D/g,''),0).toLocaleString(),
            c = n.replace(/,/g ,'.') == "NaN" ? "" : n.replace(/,/g ,'.'),
            cur = type === "text" ? this.state.invoice.currency: "";
        //c1 = type ==="text" ? c1 : "";
        return min+cur+c+c1;
    }
    componentDidMount(){
        this.unsub  = this.ref.onSnapshot(this.docUpdate);
        
    }

    render(){
        const { subPage } = this.props.match.params;
        console.log( typeof(this.state.data));
        return(
            <>
                <div className="section">
                    
                    <Breadcrumb>
                        <Breadcrumb.Item>
                            Invoice
                        </Breadcrumb.Item>
                        <Breadcrumb.Item>
                            Invoice Sender
                        </Breadcrumb.Item>
                    </Breadcrumb>
                    <Title level={2}>Invoice Sender</Title>
                </div>
                <div className="section">
                    <Table dataSource={this.state.data} columns={this.column} 
                    
                    />
                </div>
                
            </>
        )
    }
}

export default DataSender;