import React, { Component } from "react";
import Title from "antd/lib/typography/Title";
import { Link } from "react-router-dom";
import $ from "jquery";
import moment from "moment";
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import { Select } from "antd";

const { Option } = Select;

const currency = (v=0,type="input") => {
  var b = v.toString().split('.'),
      val = b[0],
      c1 = b.length <= 1 ? ",00" : b[1]=="00"? ",00":","+b[1],
      min = v < 0 ? '-':'',
      n = parseInt(val.replace(/\D/g,''),0).toLocaleString(),
      c = n.replace(/,/g ,'.') == "NaN" ? "" : n.replace(/,/g ,'.'),
      cur = type === "text" ? this.state.invoice.currency: "";
  //c1 = type ==="text" ? c1 : "";
  return min+cur+c+c1;
}

class Chart2 extends Component{
    data=[];
    state = {
      currentYear: moment().format('YYYY'),
      count : 0,
      year: [],
      option : {
        chart: {
          type: 'column'
        },
        title: {
          text: ''
        },
        subtitle: {
          text: ''
        },
        xAxis: {
          gridLineWidth: 0,
          categories: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
          ],
          labels: {
            style: {
              fontSize: '12px',
              color: '#000',
            }
          }
        },
        yAxis: {
          min: 0,
          gridLineWidth: 1,
          title: {
            text: ''
          },
          labels: {
            overflow: 'justify',
            style: {
              fontSize: '11',
            }
          }

        },
        credits: {
          enabled: false
        },
        legend: {
          enabled: false
        },
        tooltip: {
          backgroundColor: '#000000',
          borderRadius: 4,
          borderWidth: 0,
          formatter: function() {
              return '<div class="custom-tooltip"><b>' + currency(this.y) + '</b> <br> Total invoice value</div>'
          },
          useHTML: true
        },
        series: [{
          color: '#00B5D1',
          data: [0,0,0,0,0,0,0,0,0,0,0,0],
          pointWidth: 35,

        }],
        labels: {
            overflow: 'justify',
            style: {
              fontSize: '0',
            }
          }
      }
    };
    


    constructor(props){
        super(props);
        
    }

    filterByYear(y){
        const datas = [0,0,0,0,0,0,0,0,0,0,0,0];
        var maxData=0;
        
        
    
        this.props.data.forEach((doc) => {
          const {created} = doc;
          var date = moment(created.seconds*1000),
              year = date.format('YYYY'),
              month = date.format('M');
          if(year == y){
            datas[month-1] +=parseFloat(doc.total);
          }
          
        });
        maxData = Math.max.apply(null,datas)+3;
        //console.log(maxData);
        this.setState({
          option : {
            series: [{
              data: datas
            }],
            yAxis : {
              max: maxData
            },
            xAxis : {
              title : {
                text: y
              }
            }
          }
       });
    
        
      }

    dateFormat = (date, format = "MM/DD/YYYY") =>{
        return moment(date.seconds*1000).format(format)
    }

    hasValue = (array,val) => {
        //console.log('check '+val+" = "+$.inArray(val,array ));
        if($.inArray(val,array) !== -1 ){
          return true;
        }else{
          return false;
        }
    }

    docUpdate = () =>{
        const currentYear = moment().format('YYYY'),d = [],years=[];
        var i =0;
        this.props.data.forEach(doc => {
            const {created,emailFrom,billFrom,currencyCode,countryFrom,total,phoneFrom} = doc;
            var date = moment(created.seconds*1000),
                year = date.format('YYYY');
            var yy =!this.hasValue(years,year) ? years.push(year) : false;
            i++;
            d.push({
                key: doc.id,
                created
            })
            
        });
        this.setState({
            data : d,
            year: years,
            count: currency(i),
            currentYear: currentYear
         });
        this.filterByYear(currentYear);
        //this.setState({data:data})
    }
    componentDidMount(){
        this.docUpdate()
    }
    

    
    

    render(){
        //console.log(this.state.currentYear);
        return(
            <>
                <div className="chart-title">
                    <Title level={5}>Total invoice value</Title>
                    <Select defaultValue={this.state.currentYear} style={{width: 200}} size="large" onChange={v=>{this.filterByYear(v)}}>
                        {Object.keys(this.state.year).map(function(i){
                            const y = this.state.year[i];
                        return (<Option value={y} key={i}>{y}</Option>)
                        }.bind(this))}
                    </Select>
                </div>
                <div className="chart-small">
                <HighchartsReact highcharts={Highcharts} options={this.state.option} allowChartUpdate={true}  />
                </div>
                
                
            </>
        )
    }
}

export default Chart2;