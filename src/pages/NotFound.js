
import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect,Link } from "react-router-dom";

import $ from "jquery";
import Title from "antd/lib/typography/Title";

require('bootstrap')


class NotFound extends Component {
  

  render() {
    
      return (
        <main className="not-found">
            <div className="wrap">
            <img src="/asset/images/404.svg" />
            <Title>404</Title>
            <p>Sorry, the page you visited does not exist.</p>
            <Link to="/" className="btn">Back to Home</Link>
            </div>
            
        </main>

        
      );
  }
}

function mapStateToProps(state) {
  return {
    isLoggingIn: state.auth.isLoggingIn,
    loginError: state.auth.loginError,
    isAuthenticated: state.auth.isAuthenticated
  };
}

export default (connect(mapStateToProps)(NotFound));