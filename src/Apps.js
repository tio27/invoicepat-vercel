import React, {Suspense, lazy, useEffect, useState } from "react";

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { connect } from "react-redux";
import { Spin } from "antd";

const Home = lazy(()=> import('./pages/Home'));
const Main = lazy(()=> import('./pages/admin/Main'));
const Login = lazy(()=> import('./pages/Login'));
const NotFound = lazy(()=> import('./pages/NotFound'));
const ProtectedRoute = lazy(()=> import('./components/ProtectedRoute'));

function App(props) {
  const { isAuthenticated, isVerifying } = props;
  console.log(isAuthenticated);
  return (
    <Router>
      <Suspense fallback={<div className="page-loading full "><Spin size="large" /></div>}>
      <Switch>
          <ProtectedRoute isAuthenticated={isAuthenticated} isVerifying={isVerifying} path="/admin/:mainPage?/:subPage?/:sub2Page?" component={Main} />
          <Route exact path="/login" component={Login}  />
          <Route exact path="/" component={Home}  />
          <Route component={NotFound} />
        </Switch>
      </Suspense>
    </Router>
  );
}

function mapStateToProps(state) {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    isVerifying: state.auth.isVerifying
  };
}

export default connect(mapStateToProps)(App);